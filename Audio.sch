EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5E973166
P 3750 3850
AR Path="/5E973166" Ref="#PWR?"  Part="1" 
AR Path="/5E966491/5E973166" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 3750 3600 50  0001 C CNN
F 1 "GND" H 3755 3677 50  0000 C CNN
F 2 "" H 3750 3850 50  0001 C CNN
F 3 "" H 3750 3850 50  0001 C CNN
	1    3750 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1900 1350 1900
Wire Wire Line
	1800 2500 1350 2500
Wire Wire Line
	1800 3100 1350 3100
Wire Wire Line
	1800 3700 1350 3700
Wire Wire Line
	3600 3700 3750 3700
Wire Wire Line
	3750 3700 3750 3850
Wire Wire Line
	2100 1900 2250 1900
Wire Wire Line
	2100 2500 2250 2500
Wire Wire Line
	2100 3100 2250 3100
Wire Wire Line
	2700 3100 2700 2950
Wire Wire Line
	2550 3100 2700 3100
Wire Wire Line
	2700 2500 2700 2350
Wire Wire Line
	2550 2500 2700 2500
Wire Wire Line
	2700 1900 2550 1900
Wire Wire Line
	2700 2050 2700 1900
Connection ~ 2700 2500
Wire Wire Line
	2700 2650 2700 2500
Connection ~ 2700 3100
Wire Wire Line
	2700 3250 2700 3100
Wire Wire Line
	3150 3700 3300 3700
Wire Wire Line
	2700 3700 2850 3700
Wire Wire Line
	2700 3550 2700 3700
Connection ~ 2700 3700
Wire Wire Line
	2550 3700 2700 3700
Wire Wire Line
	2100 3700 2250 3700
$Comp
L Device:R R?
U 1 1 5E973187
P 2700 2200
AR Path="/5E973187" Ref="R?"  Part="1" 
AR Path="/5E966491/5E973187" Ref="R9"  Part="1" 
F 0 "R9" H 2770 2246 50  0000 L CNN
F 1 "1K" H 2770 2155 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2630 2200 50  0001 C CNN
F 3 "~" H 2700 2200 50  0001 C CNN
	1    2700 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E97318D
P 2700 2800
AR Path="/5E97318D" Ref="R?"  Part="1" 
AR Path="/5E966491/5E97318D" Ref="R10"  Part="1" 
F 0 "R10" H 2770 2846 50  0000 L CNN
F 1 "1K" H 2770 2755 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2630 2800 50  0001 C CNN
F 3 "~" H 2700 2800 50  0001 C CNN
	1    2700 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E973193
P 1950 1900
AR Path="/5E973193" Ref="R?"  Part="1" 
AR Path="/5E966491/5E973193" Ref="R1"  Part="1" 
F 0 "R1" V 1743 1900 50  0000 C CNN
F 1 "1K" V 1834 1900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1880 1900 50  0001 C CNN
F 3 "~" H 1950 1900 50  0001 C CNN
	1    1950 1900
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E973199
P 2400 1900
AR Path="/5E973199" Ref="R?"  Part="1" 
AR Path="/5E966491/5E973199" Ref="R5"  Part="1" 
F 0 "R5" V 2193 1900 50  0000 C CNN
F 1 "1K" V 2284 1900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2330 1900 50  0001 C CNN
F 3 "~" H 2400 1900 50  0001 C CNN
	1    2400 1900
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E97319F
P 1950 2500
AR Path="/5E97319F" Ref="R?"  Part="1" 
AR Path="/5E966491/5E97319F" Ref="R2"  Part="1" 
F 0 "R2" V 1743 2500 50  0000 C CNN
F 1 "1K" V 1834 2500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1880 2500 50  0001 C CNN
F 3 "~" H 1950 2500 50  0001 C CNN
	1    1950 2500
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E9731A5
P 2400 2500
AR Path="/5E9731A5" Ref="R?"  Part="1" 
AR Path="/5E966491/5E9731A5" Ref="R6"  Part="1" 
F 0 "R6" V 2193 2500 50  0000 C CNN
F 1 "1K" V 2284 2500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2330 2500 50  0001 C CNN
F 3 "~" H 2400 2500 50  0001 C CNN
	1    2400 2500
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E9731AB
P 1950 3100
AR Path="/5E9731AB" Ref="R?"  Part="1" 
AR Path="/5E966491/5E9731AB" Ref="R3"  Part="1" 
F 0 "R3" V 1743 3100 50  0000 C CNN
F 1 "1K" V 1834 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1880 3100 50  0001 C CNN
F 3 "~" H 1950 3100 50  0001 C CNN
	1    1950 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E9731B1
P 2400 3100
AR Path="/5E9731B1" Ref="R?"  Part="1" 
AR Path="/5E966491/5E9731B1" Ref="R7"  Part="1" 
F 0 "R7" V 2193 3100 50  0000 C CNN
F 1 "1K" V 2284 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2330 3100 50  0001 C CNN
F 3 "~" H 2400 3100 50  0001 C CNN
	1    2400 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E9731B7
P 1950 3700
AR Path="/5E9731B7" Ref="R?"  Part="1" 
AR Path="/5E966491/5E9731B7" Ref="R4"  Part="1" 
F 0 "R4" V 1743 3700 50  0000 C CNN
F 1 "1K" V 1834 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1880 3700 50  0001 C CNN
F 3 "~" H 1950 3700 50  0001 C CNN
	1    1950 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E9731BD
P 2700 3400
AR Path="/5E9731BD" Ref="R?"  Part="1" 
AR Path="/5E966491/5E9731BD" Ref="R11"  Part="1" 
F 0 "R11" H 2770 3446 50  0000 L CNN
F 1 "1K" H 2770 3355 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2630 3400 50  0001 C CNN
F 3 "~" H 2700 3400 50  0001 C CNN
	1    2700 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E9731C3
P 3450 3700
AR Path="/5E9731C3" Ref="R?"  Part="1" 
AR Path="/5E966491/5E9731C3" Ref="R13"  Part="1" 
F 0 "R13" V 3243 3700 50  0000 C CNN
F 1 "1K" V 3334 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3380 3700 50  0001 C CNN
F 3 "~" H 3450 3700 50  0001 C CNN
	1    3450 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E9731C9
P 3000 3700
AR Path="/5E9731C9" Ref="R?"  Part="1" 
AR Path="/5E966491/5E9731C9" Ref="R12"  Part="1" 
F 0 "R12" V 2793 3700 50  0000 C CNN
F 1 "1K" V 2884 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2930 3700 50  0001 C CNN
F 3 "~" H 3000 3700 50  0001 C CNN
	1    3000 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E9731CF
P 2400 3700
AR Path="/5E9731CF" Ref="R?"  Part="1" 
AR Path="/5E966491/5E9731CF" Ref="R8"  Part="1" 
F 0 "R8" V 2193 3700 50  0000 C CNN
F 1 "1K" V 2284 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2330 3700 50  0001 C CNN
F 3 "~" H 2400 3700 50  0001 C CNN
	1    2400 3700
	0    1    1    0   
$EndComp
Text Notes 1350 1600 0    50   ~ 0
4 BIT DAC
Text GLabel 1350 1900 0    50   Input ~ 0
DAC_B3
Text GLabel 1350 2500 0    50   Input ~ 0
DAC_B2
Text GLabel 1350 3100 0    50   Input ~ 0
DAC_B1
Text GLabel 1350 3700 0    50   Input ~ 0
DAC_B0
Text GLabel 3350 1900 2    50   Output ~ 0
DAC_OUT
Wire Wire Line
	2700 1900 3350 1900
Connection ~ 2700 1900
$Comp
L Connector:AudioJack3 J5
U 1 1 5E986BD5
P 4750 1850
F 0 "J5" H 4732 2175 50  0000 C CNN
F 1 "AudioJack3" H 4732 2084 50  0000 C CNN
F 2 "Connectors:Stereo_Jack_3.5mm_Switch_Ledino_KB3SPRS" H 4750 1850 50  0001 C CNN
F 3 "~" H 4750 1850 50  0001 C CNN
	1    4750 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1750 5500 1750
Wire Wire Line
	4950 1850 5500 1850
Wire Wire Line
	4950 1950 5500 1950
Text GLabel 5500 1750 2    50   Input ~ 0
GND
Text GLabel 5500 1850 2    50   Input ~ 0
DAC_OUT
Text GLabel 5500 1950 2    50   Input ~ 0
DAC_OUT
$Comp
L EE319K:MC34119 U?
U 1 1 5E9915F3
P 7900 2050
AR Path="/5E9915F3" Ref="U?"  Part="1" 
AR Path="/5E966491/5E9915F3" Ref="U1"  Part="1" 
F 0 "U1" H 7900 2731 50  0000 C CNN
F 1 "MC34119" H 7900 2640 50  0000 C CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 7900 2050 50  0001 C CNN
F 3 "" H 7900 2050 50  0001 C CNN
	1    7900 2050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
