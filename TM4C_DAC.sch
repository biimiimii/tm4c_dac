EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date "19 oct 2012"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR01
U 1 1 5080AA99
P 9150 2300
F 0 "#PWR01" H 9150 2050 50  0001 C CNN
F 1 "GND" H 9150 2150 50  0000 C CNN
F 2 "" H 9150 2300 50  0000 C CNN
F 3 "" H 9150 2300 50  0000 C CNN
	1    9150 2300
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR02
U 1 1 5080AA9F
P 9150 750
F 0 "#PWR02" H 9150 600 50  0001 C CNN
F 1 "VCC" H 9150 900 50  0000 C CNN
F 2 "" H 9150 750 50  0000 C CNN
F 3 "" H 9150 750 50  0000 C CNN
	1    9150 750 
	1    0    0    -1  
$EndComp
$Comp
L boosterpack:Ti_Booster_40_J1 J1
U 1 1 5080DB5C
P 9750 1250
F 0 "J1" H 9700 1900 60  0000 C CNN
F 1 "TI_BOOSTER_40_J1" H 9750 600 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" V 10200 1250 60  0001 C CNN
F 3 "" H 9750 1250 60  0001 C CNN
	1    9750 1250
	1    0    0    -1  
$EndComp
$Comp
L boosterpack:Ti_Booster_40_J2 J2
U 1 1 5080DBF4
P 9750 2750
F 0 "J2" H 9700 3400 60  0000 C CNN
F 1 "TI_BOOSTER_40_J2" H 9750 2100 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" V 10200 2750 60  0001 C CNN
F 3 "" H 9750 2750 60  0001 C CNN
	1    9750 2750
	1    0    0    -1  
$EndComp
$Comp
L boosterpack:Ti_Booster_40_J3 J3
U 1 1 5080DC03
P 9750 4300
F 0 "J3" H 9700 4950 60  0000 C CNN
F 1 "TI_BOOSTER_40_J3" H 9750 3650 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" V 10200 4300 60  0001 C CNN
F 3 "" H 9750 4300 60  0001 C CNN
	1    9750 4300
	1    0    0    -1  
$EndComp
$Comp
L boosterpack:Ti_Booster_40_J4 J4
U 1 1 5080DC12
P 9750 5850
F 0 "J4" H 9700 6500 60  0000 C CNN
F 1 "TI_BOOSTER_40_J4" H 9750 5200 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" V 10200 5850 60  0001 C CNN
F 3 "" H 9750 5850 60  0001 C CNN
	1    9750 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5080DC79
P 9150 3950
F 0 "#PWR03" H 9150 3700 50  0001 C CNN
F 1 "GND" H 9150 3800 50  0000 C CNN
F 2 "" H 9150 3950 50  0000 C CNN
F 3 "" H 9150 3950 50  0000 C CNN
	1    9150 3950
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5080DC8B
P 9150 3850
F 0 "#PWR04" H 9150 3700 50  0001 C CNN
F 1 "+5V" H 9150 3990 50  0000 C CNN
F 2 "" H 9150 3850 50  0000 C CNN
F 3 "" H 9150 3850 50  0000 C CNN
	1    9150 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 1000 8500 1000
Wire Wire Line
	9150 1100 8500 1100
$Sheet
S 1000 1000 1500 1500
U 5E966491
F0 "Audio" 50
F1 "Audio.sch" 50
$EndSheet
Text GLabel 8500 1100 0    50   Input ~ 0
DAC_B1
Text GLabel 8500 2400 0    50   Input ~ 0
DAC_B2
Text GLabel 8500 5600 0    50   Input ~ 0
DAC_B3
Text GLabel 8500 1000 0    50   Input ~ 0
DAC_B0
Wire Wire Line
	8500 2400 9150 2400
Wire Wire Line
	9150 5600 8500 5600
$EndSCHEMATC
